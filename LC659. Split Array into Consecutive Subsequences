class Solution {
/*Example: 1 2 3 3 4 4 5 5
We have counts of
1, 1 2, 1 3,2 4,2 5,2

The current element is 1, we know that in order to fit this into a sequence, either there has to be a flag set (previous size 3 sequence or 1+1 and 1+2 must have have a positive count). 2 and 3 have a positive count so, we use up these 3 values and set a flag in our other hashmap at 1+3 (4 meaning that at 4 we have the option of using this subsequence).

Now we have
1,0 2,0 3,1 4,2 5,2

4,1
We continue until 3. We see that there are no possible sequences so we check 3+1 and 3+2, 4 and 5 both have positive counts. So we take those, and add 6,1
Count HashMap:1,0 2,0 3,0 4,1 5,1
Possibility (previous valid subsequences) : 4,1 6,1

We're on 4 now, we see that we have the option of continueing a subsequence from 4,1 so we continue it and use this number. We add 5,1 since the new subsequence will end at 4 (next would be 5).
Count HashMap: 1,0 2,0 3,0 4,0 5,1
Possibility (previous valid subsequences) : 5,1 6,1

Again, on 5 we see we have 5,1 so we can use that and it is a valid input.
Count HashMap: 1,0 2,0 3,0 4,0 5,0
Possibility (previous valid subsequences) : 6,2
*/
    public boolean isPossible(int[] nums) {
        Map<Integer, Integer> canBeUse = new HashMap<>();
        Map<Integer, Integer> counts = new HashMap<>();
        for(int num: nums){
            counts.put(num, counts.getOrDefault(num, 0) + 1);
        }
        
        for(int num: nums){
            if(counts.get(num) == 0)
                continue;
            
            if(canBeUse.getOrDefault(num, 0) > 0){     //4, 1 can be used as 1, 2, 3
                canBeUse.put(num, canBeUse.getOrDefault(num, 0) - 1);
                canBeUse.put(num + 1, canBeUse.getOrDefault(num + 1, 0) + 1);
            }else if(counts.getOrDefault(num + 1, 0) > 0 && counts.getOrDefault(num + 2, 0) > 0){
                canBeUse.put(num + 3, canBeUse.getOrDefault(num + 3, 0) + 1);
                counts.put(num + 1, counts.getOrDefault(num + 1, 0) - 1);
                counts.put(num + 2, counts.getOrDefault(num + 2, 0) - 1);
            }else
                return false;
            
            counts.put(num, counts.get(num) - 1);
        }
        
        return true;
    }
}